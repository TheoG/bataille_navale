package fr.batailleMetier;

import java.io.Serializable;

/**
 * @author Maxime et Th�o
 */
public class Reserve implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int nbBateau;
	private Bateau[] flote;
	
	// Constructeur
	
	/**
	 * Initialise la reserve de bateau en fonction des r�gles du jeux
	 * @param regles
	 */
	public Reserve(Regles regles) {
		int nbBateau2 = regles.getNbBateau_2();
		int nbBateau3 = regles.getNbBateau_3();
		int nbBateau4 = regles.getNbBateau_4();
		int nbBateau5 = regles.getNbBateau_5();
		nbBateau = nbBateau2 + nbBateau3 + nbBateau4 + nbBateau5;
		flote = new Bateau[nbBateau];
		for (int i = 0; i < nbBateau; i++) {
			if (nbBateau2 > 0) {
				flote[i] = new Bateau(2);
				nbBateau2--;
			}
			else if (nbBateau3 > 0) {
				flote[i] = new Bateau(3);
				nbBateau3--;
			}
			else if (nbBateau4 > 0) {
				flote[i] = new Bateau(4);
				nbBateau4--;
			}
			else if (nbBateau5 > 0) {
				flote[i] = new Bateau(5);
				nbBateau5--;
			}
		}
	}

	// Ensemble des getteurs et setteurs
	
	public int getNbBateau() {
		return nbBateau;
	}
	public Bateau getBateau(int indice) {
		return flote[indice];
	}
	
	// Autres M�thodes
	
	/**
	 * Calcule et indique la vie de toute la reserve. (la vie de chaque bateau additionn�)
	 * @return la vie de la reserve
	 */
	public int vie() {
		int vie = 0;
		for (int i = 0; i < nbBateau; i++)
			vie += flote[i].getVie();
		return vie;
	}
	
	/**
	 * Indique si la reserve est plac� ou non. (si tous les bateaux sont plac�s)
	 * @return vrai si plac�, faux si non
	 */
	public boolean estPlace() {
		boolean place = true;
		int i = 0;
		while (i < nbBateau && place) {
			if (!flote[i].getEstPlace())
				place = false;
			i++;
		}
		return place;
	}
	
	/**
	 * Indique le nombre de bateau non plac�
	 * @return le nombre de bateau
	 */
	public int nbBateauNonPlace() {
		int nombre = 0;
		for (int i = 0; i < nbBateau; i++) {
			if (!flote[i].getEstPlace())
				nombre++;
		}
		return nombre;
	}

	/**
	 * Indique le nombre de bateau d'une taille pr�cis�e non plac�
	 * @param cases : la taille
	 * @return le nombre de bateau
	 */
	public int nbBateauNonPlace(int cases) {
		int nombre = 0;
		for (int i = 0; i < nbBateau; i++) {
			if (flote[i].getTaille() == cases && !flote[i].getEstPlace())
				nombre++;
		}
		return nombre;
	}
	
	/**
	 * Indique l'indice d'un bateau d'une taille pr�cis�e non plac�
	 * @param cases : la taille
	 * @return l'indice d'un bateau
	 */
	public int indiceBateauNonPlace(int cases) {
		for (int i = 0; i < nbBateau; i++) {
			if (flote[i].getTaille() == cases && !flote[i].getEstPlace())
				return i;
		}
		return -1;
	}
	
	/**
	 * Place un bateau � partir de la reserve
	 * @param indice : l'indice du bateau � placer
	 * @param colonne : la colonne � laquelle placer le bateau
	 * @param ligne : la ligne � laquelle placer le bateau
	 * @param direction : la direction dans laquelle le bateau sera
	 */
	public void placeBateau(int indice, int colonne, int ligne, int direction) {
		flote[indice].setPos(colonne, ligne);
		flote[indice].setDirection(direction);
		flote[indice].setEstPlace(true);
	}
	
	/**
	 * D�place un bateau � partir de la reserve 
	 * @param indice : l'indice du bateau � d�placer
	 * @param colonne : la colonne � laquelle d�plcaer le bateau
	 * @param ligne : la ligne � laquelle d�placer le bateau
	 */
	public void deplaceBateau(int indice, int colonne, int ligne) {
		flote[indice].setPos(colonne, ligne);
	}

	/**
	 * Tourne un bateau � partir de la reserve
	 * @param indice : l'indice du bateau � tourner
	 * @param direction : la direction dans laquelle le bateau sera
	 */
	public void tourneBateau(int indice, int direction) {
		flote[indice].setDirection(direction);
	}

	/**
	 * Retire un bateau du plateau � partir de la r�serve
	 * @param indice : l'indice du bateau � retirer
	 */
	public void retireBateau(int indice) {
		flote[indice].setEstPlace(false);
	}
}
