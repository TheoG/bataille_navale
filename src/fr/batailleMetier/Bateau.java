package fr.batailleMetier;

import java.io.Serializable;

/**
 * @author Th�o et Maxime
 */
public class Bateau implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private int taille;
	private int vie;
	private int ligne;
	private int colonne;
	private int direction;
	private boolean estPlace;

	// Constructeur

	public Bateau(int taille) {
		this.taille = taille;
		vie = taille;
		estPlace = false;
	}
	
	// Ensemble des getteurs et setteurs
	
	public int getTaille() {
		return taille;
	}
	public int getVie() {
		return vie;
	}
	public int getLigne() {
		return ligne;
	}
	public int getColonne() {
		return colonne;
	}
	public void setPos(int colonne, int ligne) {
		this.colonne = colonne;
		this.ligne = ligne;
	}
	public int getDirection() {
		return direction;
	}
	public void setDirection(int direction) {
		this.direction = direction;
	}
	public boolean getEstPlace() {
		return estPlace;
	}	
	public void setEstPlace(boolean estPlace) {
		this.estPlace = estPlace;
	}
	
	// Autres M�thodes
	
	/**
	 * le bateau vient de se faire toucher et baisse sa vie en cons�quence
	 */
	public void seFaitToucher() {
		vie--;
	}
}
