package fr.batailleMetier;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Th�o et Maxime
 */
public class Joueur implements Serializable {
	private static final long serialVersionUID = 1L;
	private String nom;
	private Plateau plateau;
	private Joueur ennemi;
	private boolean estSonTour;
	private boolean estGagnant;
	private Collection<Boolean> listeCoups = new ArrayList<Boolean>();

	// Constructeur
	
	/**
	 * Initialise le joueur en ofnction des r�gles
	 * @param regles
	 */
	public Joueur(Regles regles) {
		plateau = new Plateau(regles);
	}
	
	// Ensemble des getteurs et setteurs
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Plateau getPlateau() {
		return plateau;
	}
	public Joueur getEnnemi() {
		return ennemi;
	}
	public void setEnnemi(Joueur ennemi) {
		this.ennemi = ennemi;
	}
	public boolean getEstSonTour() {
		return estSonTour;
	}
	public void setEstSonTour(boolean estSonTour) {
		this.estSonTour = estSonTour;
	}
	public boolean getEstGagnant() {
		return estGagnant;
	}
	public void setEstGagnant(boolean estGagnant) {
		this.estGagnant = estGagnant;
	}
	
	// Autres m�thodes
	
	/**
	 * Calcule le score d'un joueur
	 * @return le score
	 */
	public int calculerScore() {
		int score = 0;
		int combo = 0;
		
		for (Boolean i : listeCoups) {
			if (i) {
				score += 10 + combo;
				combo += 10;
			}
			else
				combo = 0;
		}
		return score;
	}
	
	/**
	 * Ajoute un tir dans la liste des coups
	 * @param tir
	 */
	public void ajouterTir(boolean tir) {
		listeCoups.add(tir);
	}
	
	/**
	 * Rep�re l'indice du bateau pos� sur des coordonn�es
	 * @param ligneFixe : ligne selectionn�e
	 * @param colonneFixe : colonne selectionn�e
	 * @return l'indice du bateau
	 */
	public int repere(int ligneFixe, int colonneFixe) {
		Bateau bateau;
		boolean trouve = false;
		int indice = 0;
		int i = 0;
		
		while (indice < plateau.getReserve().getNbBateau() && !trouve) {
			i = 0;
			bateau = plateau.getReserve().getBateau(indice);
			while (i < bateau.getTaille() && !trouve && bateau.getEstPlace()) {
				switch (bateau.getDirection()) {
				case 0 :
					if (bateau.getLigne() == ligneFixe && bateau.getColonne() + i == colonneFixe)
						trouve = true;
					break;
				case 1 :
					if (bateau.getLigne() + i == ligneFixe && bateau.getColonne() == colonneFixe)
						trouve = true;
					break;
				case 2 :
					if (bateau.getLigne() == ligneFixe && bateau.getColonne() - i == colonneFixe)
						trouve = true;
					break;
				case 3 :
					if (bateau.getLigne() - i == ligneFixe && bateau.getColonne() == colonneFixe)
						trouve = true;
					break;
				default :
				}
				i++;
			}
			indice++;
		}
		if (trouve)
			return indice - 1;
		return -1;
	}
	
	/**
	 * Verifie si un bateau peut se placer � un endroit
	 * @param indice : l'indice du bateau
	 * @param taille : la taille du bateau
	 * @param colonne : la colonne hypot�tique du bateau
	 * @param ligne : la ligne hypot�tique du bateau
	 * @param direction : la direction hypot�tique du bateau
	 * @return vrai si le placement est possible, faux si non
	 */
	public boolean verif(int indice, int taille, int colonne, int ligne, int direction) {
		int taillePlateau = plateau.getTaille();
		boolean dispo = true;
		int i = 0;
		switch (direction) {
		case 0 :
			if (colonne + taille - 1 > taillePlateau - 1 || colonne < 0 || ligne > taillePlateau - 1 || ligne < 0)
				dispo = false;
			while (i < taille && dispo) {
				if (plateau.contenuCase(ligne, colonne + i) != 0 && indice != repere(ligne, colonne + i))
					dispo = false;
				i++;
			}
			break;
		case 1 :
			if (colonne > taillePlateau - 1 || colonne < 0 || ligne + taille - 1 > taillePlateau - 1 || ligne < 0)
				dispo = false;
			while (i < taille && dispo) {
				if (plateau.contenuCase(ligne + i, colonne) != 0 && indice != repere(ligne + i, colonne))
					dispo = false;
				i++;
			}
			break;
		case 2 :
			if (colonne - taille + 1 < 0 || colonne > taillePlateau - 1 || ligne > taillePlateau - 1 || ligne < 0)
				dispo = false;
			while (i < taille && dispo) {
				if (plateau.contenuCase(ligne, colonne - i) != 0 && indice != repere(ligne, colonne - i))
					dispo = false;
				i++;
			}
			break;
		case 3 :
			if (ligne - taille + 1 < 0 || ligne > taillePlateau - 1 || colonne > taillePlateau - 1 || colonne < 0)
				dispo = false;
			while (i < taille && dispo) {
				if (plateau.contenuCase(ligne - i, colonne) != 0 && indice != repere(ligne - i, colonne))
					dispo = false;
				i++;
			}
			break;
		default :
		}
		
		return dispo;
	}
}