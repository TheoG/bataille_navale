package fr.batailleMetier;

/**
 * @author Th�o et Maxime
 */
public class Regles {
	private int taille_plateau;
	private int nbBateau_total;
	private int nbBateau_2;
	private int nbBateau_3;
	private int nbBateau_4;
	private int nbBateau_5;
	private String nomJoueur1;
	private String nomJoueur2;
	private boolean IA;
	
	// Constructeur
	
	public Regles() {
		taille_plateau = 10;
		nbBateau_total = 5;
		nbBateau_2 = 1;
		nbBateau_3 = 2;
		nbBateau_4 = 1;
		nbBateau_5 = 1;
		nomJoueur1 = "Joueur 1";
		nomJoueur2 = "Joueur 2";
		IA = false;
	}

	// Ensemble des getteurs et setteurs
	
	public int getTaille_plateau() {
		return taille_plateau;
	}
	public void setTaille_plateau(int taille_plateau) {
		this.taille_plateau = taille_plateau;
	}
	public int getNbBateau_total() {
		return nbBateau_total;
	}
	public void setNbBateau_total(int nbBateau_total) {
		this.nbBateau_total = nbBateau_total;
	}
	public int getNbBateau_2() {
		return nbBateau_2;
	}
	public void setNbBateau_2(int nbBateau_2) {
		this.nbBateau_2 = nbBateau_2;
	}
	public int getNbBateau_3() {
		return nbBateau_3;
	}
	public void setNbBateau_3(int nbBateau_3) {
		this.nbBateau_3 = nbBateau_3;
	}
	public int getNbBateau_4() {
		return nbBateau_4;
	}
	public void setNbBateau_4(int nbBateau_4) {
		this.nbBateau_4 = nbBateau_4;
	}
	public int getNbBateau_5() {
		return nbBateau_5;
	}
	public void setNbBateau_5(int nbBateau_5) {
		this.nbBateau_5 = nbBateau_5;
	}
	public String getNomJoueur1() {
		return nomJoueur1;
	}
	public void setNomJoueur1(String nomJoueur1) {
		this.nomJoueur1 = nomJoueur1;
	}
	public String getNomJoueur2() {
		return nomJoueur2;
	}
	public void setNomJoueur2(String nomJoueur2) {
		this.nomJoueur2 = nomJoueur2;
	}
	public boolean isIA() {
		return IA;
	}
	public void setIA(boolean iA) {
		IA = iA;
	}
}
