package fr.batailleMetier;

import java.io.Serializable;

/**
 * @author Maxime et Théo
 */
public class Case implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private int contenu;
	private boolean estTouche;
	
	// Constructeur
	
	public Case() {
		contenu = 0;
		estTouche = false;
	}

	// Ensemble des getteur et setteurs

	public int getContenu() {
		return contenu;
	}
	public void setContenu(int contenu) {
		//contenu = (	0 : mer, 4 : bateau touché
		//				1 : bateau horizontal, 2 : bout droit, 3 : bout gauche,
		//				-1 : bateau vertical, -2 : bout bas, -3 : bout haut)
		this.contenu = contenu;
	}
	public boolean getEstTouche() {
		return estTouche;
	}
	public void setEstTouche(boolean estTouche) {
		this.estTouche = estTouche;
	}
}
