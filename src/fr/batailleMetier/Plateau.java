package fr.batailleMetier;

import java.io.Serializable;

/**
 * @author Maxime et Th�o
 */
public class Plateau implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Reserve reserve;
	private int taille;
	private Case[][] terrain;
	
	// Constructeur
	
	/**
	 * Initialise le plateau en fonction des r�gles
	 * @param regles
	 */
	public Plateau(Regles regles) {
		taille = regles.getTaille_plateau();
		terrain = new Case[taille][taille];
		for(int i = 0; i < taille; i++) {
			for (int j = 0; j < taille; j++)
				terrain[i][j] = new Case();
		}
		reserve = new Reserve(regles);
	}
	
	// Enesemble des getteurs et setteurs
	public Reserve getReserve() {
		return reserve;
	}
	public int getTaille() {
		return taille;
	}
	public Case getCase(int ligne, int colonne) {
		return terrain[ligne][colonne];
	}

	// Autres m�thodes

	/**
	 * Indique le contenu d'une case
	 * @param ligne : la ligne de la case dans le plateau
	 * @param colonne : la colonne de la cas dans le plateau
	 * @return un caract�re
	 */
	public int contenuCase(int ligne, int colonne) {
		return terrain[ligne][colonne].getContenu();
	}
	
	/**
	 * Place un bateau � partir du plateau
	 * @param indice : l'indice du bateau � placer
	 * @param colonne : colonne � laquelle placer le bateau
	 * @param ligne : ligne � laquelle placer le bateau
	 * @param direction : direction dans laquelle le bateau sera
	 */
	public void placeBateau(int indice, int colonne, int ligne, int direction) {
		reserve.placeBateau(indice, colonne, ligne, direction);
		actuContenu();
	}

	/**
	 * D�place un bateau � partir du plateau
	 * @param indice : l'indice du bateau � d�placer
	 * @param colonne : colonne � laquelle d�placer le bateau
	 * @param ligne : ligne � laquelle d�placer le bateau
	 */
	public void deplaceBateau(int indice, int colonne, int ligne) {
		reserve.deplaceBateau(indice, colonne, ligne);
		actuContenu();
	}
	
	/**
	 * Tourne un bateau � partir du plateau
	 * @param indice : l'indice du bateau � tourner
	 * @param direction : direction dans laquelle le bateau sera
	 */
	public void tourneBateau(int indice, int direction) {
		reserve.tourneBateau(indice, direction);
		actuContenu();
	}

	/**
	 * Reture un bateau du palteau � partir du plateau
	 * @param indice : l'indice du bateau � retirer
	 */
	public void retireBateau(int indice) {
		reserve.retireBateau(indice);
		actuContenu();
	}
	
	/**
	 * Acutalise le contenu de chaque case du plateau
	 */
	public void actuContenu() {
		int indice, i, ligne, colonne, taille;
		int nbBateau = reserve.getNbBateau();
		Bateau bateau;
		
		for (indice = 0; indice < this.taille; indice++) {
			for (i = 0; i < this.taille; i++)
				terrain[indice][i].setContenu(0);
		}
		
		for (indice = 0; indice < nbBateau; indice++) {
			bateau = reserve.getBateau(indice);
			ligne = bateau.getLigne();
			colonne = bateau.getColonne();
			taille = bateau.getTaille();
			if (bateau.getEstPlace()) {
				switch (bateau.getDirection()) {
				case 0 :
					terrain[ligne][colonne].setContenu(3);
					for (i = colonne + 1; i < colonne + taille - 1; i++)
						terrain[ligne][i].setContenu(1);
					terrain[ligne][colonne + taille - 1].setContenu(2);
					break;
				case 1 :
					terrain[ligne][colonne].setContenu(-3);
					for (i = ligne + 1; i < ligne + bateau.getTaille() - 1; i++)
						terrain[i][colonne].setContenu(-1);
					terrain[ligne + taille - 1][colonne].setContenu(-2);
					break;
				case 2 :
					terrain[ligne][colonne].setContenu(2);
					for (i = colonne - 1; i > colonne - taille + 1; i--)
						terrain[ligne][i].setContenu(1);
					terrain[ligne][colonne - taille + 1].setContenu(3);
					break;
				case 3 :
					terrain[ligne][colonne].setContenu(-2);
					for (i = ligne - 1; i > ligne - taille + 1; i--)
						terrain[i][colonne].setContenu(-1);
					terrain[ligne - taille + 1][colonne].setContenu(-3);
					break;
				default :
					System.out.println("coucou");
				}
			}
		}
	}
}
