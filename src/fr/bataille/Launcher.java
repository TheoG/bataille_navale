package fr.bataille;

import fr.batailleCtrl.OptionCtrl;
import fr.batailleIHM.Fenetre.FenetrePrincipale;

public class Launcher {
	public static void main(String[] args) {
		@SuppressWarnings("unused")
		FenetrePrincipale window = new FenetrePrincipale(new OptionCtrl());
	}
}