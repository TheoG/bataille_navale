package fr.batailleCtrl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.batailleIHM.Fenetre.FenetrePartie;
import fr.batailleIHM.Fenetre.FenetrePlacement;
import fr.batailleMetier.Joueur;
import fr.batailleMetier.Regles;

/**
 * @author Th�o et Maxime
 */
public class MenuCtrl {
	private Regles regles;
	private Joueur joueur1;
	private Joueur joueur2;
	
	private PlacementCtrl placementCtrl;
	
	// Constructeur
	
	public MenuCtrl(Regles regles) {
		this.regles = regles;
	}
	
	// Autres m�thodes
	
	/**
	 * lance la partie "placement" de l'application
	 */
	public void lancePlacementIhm() {
		placementCtrl = new PlacementCtrl(regles);
		new FenetrePlacement(placementCtrl);
	}

	/**
	 * M�thode de sauvegarde de la partie
	 * @param sav Fichier dans lequel on effectue la sauvegarde
	 */
	public void charger(File sav) {
		FileNameExtensionFilter filter = new FileNameExtensionFilter(null, "players");
		
		if (!filter.accept(sav)) {
			JOptionPane.showMessageDialog(new JFrame(), "Le fichier que vous essayez de charger ne correspond pas � un fichier de jeu pour la Bataille Navale !", "Erreur de fichier",
				    JOptionPane.ERROR_MESSAGE);
		} else {
			try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(sav))) {
				Object joueurPotentiel1;
				Object joueurPotentiel2;
				
				joueurPotentiel1 = ois.readObject();
				joueurPotentiel2 = ois.readObject();
				if (joueurPotentiel1 instanceof Joueur && joueurPotentiel2 instanceof Joueur) {
					joueur1 = (Joueur)joueurPotentiel1;
					joueur2 = (Joueur)joueurPotentiel2;
				}
				else {
					System.out.println("j'aime les cr�pes");
					JOptionPane.showMessageDialog(new JFrame(), "Le fichier charg� ne correspond pas � un fichier de sauvegarde de la bataille navale !", "Erreur de chargement",
						    JOptionPane.ERROR_MESSAGE); 	
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		new FenetrePartie(new PartieCtrl(joueur1, joueur2, regles));
	}
}
