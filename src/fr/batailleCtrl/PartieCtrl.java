package fr.batailleCtrl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.batailleMetier.Bateau;
import fr.batailleMetier.Case;
import fr.batailleMetier.Joueur;
import fr.batailleMetier.Plateau;
import fr.batailleMetier.Regles;
import fr.batailleMetier.Reserve;

/**
 * @author Th�o et Maxime
 */
public class PartieCtrl {
	Regles regles;
	Joueur joueur1;
	Joueur joueur2;
	
	// Constructeur
	public PartieCtrl(Joueur joueur1, Joueur joueur2, Regles regles) {
		this.joueur1 = joueur1;
		this.joueur2 = joueur2;
		this.regles = regles;
	}
	
	//Autres m�thodes
	
	/**
	 * Indique le bon joueur suivant le numero pass� en param�tre
	 * @param numeroJoueur : numero du joueur entrain de tirer
	 * @return le joueur
	 */
	public Joueur joueurNumero(int numeroJoueur) {
		if (numeroJoueur == 1)
			return joueur1;
		else
			return joueur2;
	}
	
	/**
	 * Verifie si la valeur est comprise entre les bornes min et max
	 * @param valeur : la valeur � verifier
	 * @param borneMin
	 * @param borneMax
	 * @return vrai si la valeur est valide faux si non
	 */
	public boolean bonneValeur(int valeur, int borneMin, int borneMax) {
		if (valeur < borneMin || valeur > borneMax)
			return false;
		return true;
	}
	
	/**
	 * renvoit les r�gles
	 * @return les r�gles
	 */
	public Regles renvoitRegles() {
		return regles;
	}
	
	public int taillePlateau() {
		return regles.getTaille_plateau();
	}
	public int contenuCase(int numeroJoueur, int ligne, int colonne) {
		return joueurNumero(numeroJoueur).getPlateau().contenuCase(ligne, colonne);
	}
	public boolean caseEstTouche(int numeroJoueur, int ligne, int colonne) {
		return joueurNumero(numeroJoueur).getPlateau().getCase(ligne, colonne).getEstTouche();
	}
	public boolean estCoule(int numeroJoueur, int ligne, int colonne) {
		Joueur joueur = joueurNumero(numeroJoueur);
		int indice = joueur.repere(ligne, colonne);
		if (indice != -1)
			return (joueur.getPlateau().getReserve().getBateau(indice).getVie() == 0);
		else
			return false;
	}
	
	/**
	 * Indique si la partie est finie
	 * @return vrai si finie faux si non
	 */
	public boolean estFinie() {
		if (joueur1.getEstGagnant() || joueur2.getEstGagnant())
			return true;
		return false;
	}
	
	/**
	 * Indique le numero du joueur dont c'est le tour de jouer
	 * @return le numero de joueur
	 */
	public int joueurTour() {
		if (joueur1.getEstSonTour())
			return 1;
		else
			return 2;
	}
	
	/**
	 * Indique le numero du joueur dont ce n'est pas le tour de jouer
	 * @return le numero du joueur
	 */
	public int joueurNonTour() {
		if (joueur1.getEstSonTour())
			return 2;
		else
			return 1;
	}
	
	/**
	 * Indique le numero du joueur gagnant
	 * @return le numero du joueur
	 */
	public int joueurGagnant() {
		if (joueur1.getEstGagnant())
			return 1;
		else
			return 2;
	}
	
	/**
	 * Indique le nom du joueur
	 * @param numeroJoueur : le numero du joueur � indiquer
	 * @return le nom
	 */
	public String nomJoueur(int numeroJoueur) {
		return joueurNumero(numeroJoueur).getNom();
	}
	
	/**
	 * Permet de sauvegarder la partie 
	 * @param sav Fichier dans lequel on sauvegarde
	 */
	public void sauvegarde(File sav) {
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(sav + ".players"))) {
			oos.writeObject(joueur1);
			oos.writeObject(joueur2);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Permet de charger une partie � partir d'un fichier
	 * @param sav Fichier dans lequel on r�cup�re la sauvegarde
	 */
	public void charger(File sav) {
		FileNameExtensionFilter filter = new FileNameExtensionFilter(null, "players");
		
		if (!filter.accept(sav)) {
			JOptionPane.showMessageDialog(new JFrame(), "Le fichier que vous essayez de charger ne correspond pas � un fichier de jeu pour la Bataille Navale !", "Erreur de fichier",
				    JOptionPane.ERROR_MESSAGE);
		} else {
			try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(sav))) {
				Object joueurPotentiel1;
				Object joueurPotentiel2;
				
				joueurPotentiel1 = ois.readObject();
				joueurPotentiel2 = ois.readObject();
				if (joueurPotentiel1 instanceof Joueur && joueurPotentiel2 instanceof Joueur) {
					joueur1 = (Joueur)joueurPotentiel1;
					joueur2 = (Joueur)joueurPotentiel2;
				}
				else {
					System.out.println("j'aime les cr�pes");
					JOptionPane.showMessageDialog(new JFrame(), "Le fichier charg� ne correspond pas � un fichier de sauvegarde de la bataille navale !", "Erreur de chargement",
						    JOptionPane.ERROR_MESSAGE); 	
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * M�thode permettant de tirer sur l'ennemi
	 * @param numeroJoueur Joueur effectuant l'action
	 * @param ligne 
	 * @param colonne
	 * @return int en fonction de l'effet du tir
	 */
	public int tire(int numeroJoueur, int ligne, int colonne) {
		Joueur joueur = joueurNumero(numeroJoueur);
		Joueur ennemi = joueur.getEnnemi();
		Plateau plateauEnnemi = ennemi.getPlateau();
		Case caseEnnemi = plateauEnnemi.getCase(ligne, colonne);
		Reserve reserveEnnemi = plateauEnnemi.getReserve();
		Bateau bateauEnnemi;
		int indice;
		int resultat = 0;
		
		if (caseEnnemi.getContenu() != 0) {
			resultat = 1;
			indice = ennemi.repere(ligne, colonne);
			bateauEnnemi = reserveEnnemi.getBateau(indice);
			joueur.ajouterTir(true);
			if (indice != -1) {
				bateauEnnemi.seFaitToucher();
				if (bateauEnnemi.getVie() == 0)
					resultat = 2;
			}
		}
		else
			joueur.ajouterTir(false);
		caseEnnemi.setEstTouche(true);
		
		if (reserveEnnemi.vie() == 0) {
			joueur.setEstGagnant(true);
			for (int i = 0; i < regles.getTaille_plateau(); i++) {
				for (int j = 0; j < regles.getTaille_plateau(); j++) {
					plateauEnnemi.getCase(i, j).setEstTouche(true);
				}
			}
		}
		
		joueur.setEstSonTour(false);
		ennemi.setEstSonTour(true);
		
		return resultat;
	}

	/**
	 * Petite m�thode permettant de r�cup�rer le score d'un joueur � un instant t
	 * @param numeroJoueur
	 * @return Valeur du score
	 */
	public int score(int numeroJoueur) {
		return joueurNumero(numeroJoueur).calculerScore();
	}
}
