package fr.batailleCtrl;

import fr.batailleMetier.Regles;

/**
 * @author Maxime et Th�o
 */
public class OptionCtrl {
	private Regles regles;
	
	// Constructeurs
	
	public OptionCtrl() {
		this.regles = new Regles();
	}
	public OptionCtrl(Regles regles) {
		this.regles = regles;
	}
	
	// Ensemble des getteurs et setteurs
	public void setTaille(int taille) {
		if (verifValeur(taille, 8, 12))
			regles.setTaille_plateau(taille);
	}
	public void setNbBateau_total() {
		regles.setNbBateau_total(regles.getTaille_plateau() - 3);
	}
	public void setBateau2(int nbre) {
		if (verifValeur(nbre, 0, 3))
			regles.setNbBateau_2(nbre);
	}
	public void setBateau3(int nbre) {
		if (verifValeur(nbre, 0, 3))
			regles.setNbBateau_3(nbre);
	}
	public void setBateau4(int nbre) {
		if (verifValeur(nbre, 0, 3))
			regles.setNbBateau_4(nbre);
	}
	public void setBateau5(int nbre) {
		if (verifValeur(nbre, 0, 3))
			regles.setNbBateau_5(nbre);
	}
	public void setNomJoueur1(String joueur1) {
		if (!joueur1.isEmpty()) {
			regles.setNomJoueur1(joueur1);
		}
	}
	public void setNomJoueur2(String joueur2) {
		if (!joueur2.isEmpty()) {
			regles.setNomJoueur2(joueur2);
		}
	}
	public int getTaille() {
		return regles.getTaille_plateau();
	}
	public int getNbBateau() {
		return regles.getNbBateau_total();
	}
	public int getBateau2() {
		return regles.getNbBateau_2();
	}
	public int getBateau3() {
		return regles.getNbBateau_3();
	}
	public int getBateau4() {
		return regles.getNbBateau_4();
	}
	public int getBateau5() {
		return regles.getNbBateau_5();
	}
	public String getNomJoueur1() {
		return regles.getNomJoueur1();
	}
	public String getNomJoueur2() {
		return regles.getNomJoueur2();
	}
	public boolean isIA() {
		return regles.isIA();
	}
	
	// Autres m�thodes
	
	/**
	 * V�rifie que la valeur soit comprise entre la borne min et la borne max
	 * @param valeur : la valeur � v�rifier
	 * @param min : la borne min
	 * @param max : la borne max
	 * @return vrai si la valeur est bonne, fausse sinon
	 */
	public boolean verifValeur(int valeur, int min, int max) {
		if (valeur < min || valeur > max) 
			return false;
		return true;
	}
	
	/**
	 * Renvoie les regles
	 * @return les regles
	 */
	public Regles renvoieRegles() {
		return regles;
	}
}
