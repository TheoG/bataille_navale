package fr.batailleCtrl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.batailleIHM.Fenetre.FenetrePartie;
import fr.batailleMetier.Bateau;
import fr.batailleMetier.Joueur;
import fr.batailleMetier.Plateau;
import fr.batailleMetier.Regles;

/**
 * @author Maxime et Th�o
 */
public class PlacementCtrl {
	private PartieCtrl partieCtrl;
	private Regles regles;
	private Joueur joueur1;
	private Joueur joueur2;
	
	// Constructeur
	
	public PlacementCtrl(Regles regles) {
		this.regles = regles;
		this.joueur1 = new Joueur(regles);
		this.joueur2 = new Joueur(regles);
		joueur1.setNom(regles.getNomJoueur1());
		joueur2.setNom(regles.getNomJoueur2());
		joueur1.setEnnemi(joueur2);
		joueur2.setEnnemi(joueur1);
	}

	// Autres m�thodes
	
	/**
	 * Indique le bon joueur suivant le numero pass� en param�tre
	 * @param numeroJoueur : numero du joueur entrain de placer ses bateaux
	 * @return le joueur
	 */
	public Joueur joueurNumero(int numeroJoueur) {
		if (numeroJoueur == 1)
			return joueur1;
		else
			return joueur2;
	}
	
	/**
	 * Indique si le placement est validable ou non
	 * @param numeroJoueur : numero du joueur entrain de placer ses bateaux
	 * @return vrai si validable faux si non
	 */
	public boolean estValidable(int numeroJoueur) {
		Joueur joueur = joueurNumero(numeroJoueur);
		
		if (joueur.getPlateau().getReserve().estPlace())
			return true;
		else
			return false;
	}
	
	public int taillePlateau() {
		return regles.getTaille_plateau();
	}
	public int nbBateauTotal() {
		return regles.getNbBateau_2() + regles.getNbBateau_3() + regles.getNbBateau_4() + regles.getNbBateau_5();
	}
	public int nbBateauRestant(int numeroJoueur, int nbCase) {
		Joueur joueur = joueurNumero(numeroJoueur);
		return joueur.getPlateau().getReserve().nbBateauNonPlace(nbCase);
	}
	public int indiceBateauNonPlace(int numeroJoueur, int nbCase) {
		Joueur joueur = joueurNumero(numeroJoueur);
		return joueur.getPlateau().getReserve().indiceBateauNonPlace(nbCase);
	}
	public int contenuCase(int numeroJoueur, int ligne, int colonne) {
		Joueur joueur = joueurNumero(numeroJoueur);
		return joueur.getPlateau().contenuCase(ligne, colonne);
	}
	public boolean bateauEstPlace(int numeroJoueur, int indice) {
		Joueur joueur = joueurNumero(numeroJoueur);
		return joueur.getPlateau().getReserve().getBateau(indice).getEstPlace();
	}
	public boolean reserveEstPlace(int numeroJoueur) {
		Joueur joueur = joueurNumero(numeroJoueur);
		if (joueur.getPlateau().getReserve().nbBateauNonPlace() == 0)
			return true;
		else
			return false;
	}
	
	/**
	 * Renvoit les r�gles
	 * @return les r�gles
	 */
	public Regles renvoitRegles() {
		return regles;
	}

	/**
	 * Verifie si la valeur est comprise entre les bornes min et max
	 * @param valeur : la valeur � verifier
	 * @param borneMin
	 * @param borneMax
	 * @return vrai si la valeur est valide faux si non
	 */
	public boolean bonneValeur(int valeur, int borneMin, int borneMax) {
		if (valeur < borneMin || valeur > borneMax)
			return false;
		return true;
	}

	/**
	 * Indique si le joueur clique sur un bateau plac� sur le plateau
	 * @param numeroJoueur : numero du joueur entrain de placer ses bateaux
	 * @param ligne : ligne � verifi�e
	 * @param colonne : colonne � verifi�e
	 * @return vrai si il clique bien sur un bateau faux si non
	 */
	public boolean cliqueSurBateau(int numeroJoueur, int ligne, int colonne) {
		int contenu = joueurNumero(numeroJoueur).getPlateau().contenuCase(ligne, colonne);
		
		if (contenu == 1)
			return true;
		if (contenu == 2)
			return true;
		if (contenu == 3)
			return true;
		if (contenu == -1)
			return true;
		if (contenu == -2)
			return true;
		if (contenu == -3)
			return true;
		return false;
	}
	
	
	/**
	 * Place un bateau
	 * @param numeroJoueur : numero du joueur entrain de placer ses bateaux
	 * @param ligne
	 * @param colonne
	 * @param indice
	 * @return vrai si le placement s'est effectu� faux si non
	 */
	public boolean placement(int numeroJoueur, int ligne ,int colonne, int indice) {
		Joueur joueur = joueurNumero(numeroJoueur);
		Plateau plateau = joueur.getPlateau();
		Bateau bateau = plateau.getReserve().getBateau(indice);
		int taille = bateau.getTaille();
		
		if (joueur.verif(indice, taille, colonne, ligne, 0))
			plateau.placeBateau(indice, colonne, ligne, 0);

		else if (joueur.verif(indice, taille, colonne, ligne, 1))
			plateau.placeBateau(indice, colonne, ligne, 1);

		else if (joueur.verif(indice, taille, colonne, ligne, 2))
			plateau.placeBateau(indice, colonne, ligne, 2);
			
		else if (joueur.verif(indice, taille, colonne, ligne, 3))
			plateau.placeBateau(indice, colonne, ligne, 3);
		
		if (bateau.getEstPlace())
			return true;
		
		return false;
	}
	
	/**
	 * tourne un bateau
	 * @param numeroJoueur : numero du joueur entrain de placer ses bateaux
	 * @param ligne
	 * @param colonne
	 * @return vrai si la rotation s'est effectu�e faux si non
	 */
	public boolean tourne(int numeroJoueur, int ligne, int colonne) {
		Joueur joueur = joueurNumero(numeroJoueur);
		Plateau plateau = joueur.getPlateau();
		Bateau bateau;
		int taille, direction, directionFixe, indice = 0;
		
		indice = joueur.repere(ligne, colonne);
		if (indice == -1)
			return false;
		bateau = plateau.getReserve().getBateau(indice);
		directionFixe = bateau.getDirection();
		direction = directionFixe;
		ligne = bateau.getLigne();
		colonne = bateau.getColonne();
		taille = bateau.getTaille();
		
		do {
			if (direction == 0)
				direction = 1;
			else if (direction == 1)
				direction = 2;
			else if (direction == 2)
				direction = 3;
			else if (direction == 3)
				direction = 0;

			if (joueur.verif(indice, taille, colonne, ligne, direction)) {
				plateau.tourneBateau(indice, direction);
				return true;
			}
		} while (direction != directionFixe);
		return false;
	}
	
	/**
	 * Retire un bateau
	 * @param numeroJoueur : numero du joueur entrain de placer ses bateaux
	 * @param ligne
	 * @param colonne
	 * @return vrai si le retir s'est effectu� faux si non
	 */
	public boolean retire(int numeroJoueur, int ligne, int colonne) {
		Joueur joueur = joueurNumero(numeroJoueur);
		int indice = joueur.repere(ligne, colonne);
		
		if (indice != -1) {
			joueur.getPlateau().retireBateau(indice);
			return true;
		}
		return false;
	}

	/**
	 * M�thode permettant de charger une partie
	 * @param sav Fichier contenant la sauvegarde
	 */
	public void charger(File sav) {
		FileNameExtensionFilter filter = new FileNameExtensionFilter(null, "players");
		
		if (!filter.accept(sav)) {
			JOptionPane.showMessageDialog(new JFrame(), "Le fichier que vous essayez de charger ne correspond pas � un fichier de jeu pour la Bataille Navale !", "Erreur de fichier",
				    JOptionPane.ERROR_MESSAGE);
		} else {
			try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(sav))) {
				Object joueurPotentiel1;
				Object joueurPotentiel2;
				
				joueurPotentiel1 = ois.readObject();
				joueurPotentiel2 = ois.readObject();
				if (joueurPotentiel1 instanceof Joueur && joueurPotentiel2 instanceof Joueur) {
					joueur1 = (Joueur)joueurPotentiel1;
					joueur2 = (Joueur)joueurPotentiel2;
				}
				else {
					System.out.println("j'aime les cr�pes");
					JOptionPane.showMessageDialog(new JFrame(), "Le fichier charg� ne correspond pas � un fichier de sauvegarde de la bataille navale !", "Erreur de chargement",
						    JOptionPane.ERROR_MESSAGE); 	
				}
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		new FenetrePartie(new PartieCtrl(joueur1, joueur2, regles));
	}
	
	/**
	 * lance la partie "partie" de l'application
	 */
	public void lancePartieIhm() {
		joueur1.setEstSonTour(true);
		partieCtrl = new PartieCtrl(joueur1, joueur2, regles);
		new FenetrePartie(partieCtrl);
	}
	
}
