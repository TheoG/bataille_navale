package fr.batailleIHM;

import java.awt.Color;

import javax.swing.JButton;

/**
 * Class de cr�ation des boutons du menu du jeu
 *
 */
public class BoutonMenu extends JButton {
	private static final long serialVersionUID = 1820054143339002565L;

	public BoutonMenu(String nom) {
		super(nom);
		this.setBackground(new Color(30, 136, 229));
		this.setFocusable(false);
	}
}
