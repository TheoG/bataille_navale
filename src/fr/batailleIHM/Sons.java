package fr.batailleIHM;

import java.io.File;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Sons {
	private static Clip clip;
	private static Clip clip2;
	private static boolean jouer = true;
	
	public static void jouerSon(SonManager s) {
		if (jouer) {
			String son = null;
	
			switch (s) {
			case MENU:
				son = "Ressources/Sons/menuPrincipal.wav";
				break;
			case JOUER:
				son = "Ressources/Sons/bulles.wav";
				break;
			case PLACE:
				son = "Ressources/Sons/CorneBrume1.wav";
				break;
			case RETIRE:
				son = "Ressources/Sons/CorneBrume2.wav";
				break;
			case RATE:
				son = "Ressources/Sons/rate.wav";
				break;
			case TOUCHE:
				son = "Ressources/Sons/touche.wav";
				break;
			default:
				son = "Ressources/Sons/erreur.wav";
			}
	
			File audio = new File(son);
	
			try {
				clip = AudioSystem.getClip();
				clip.open(AudioSystem.getAudioInputStream(audio));
				clip.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void jouerPartie() {
		if (jouer) {
			File audio = new File("Ressources/Sons/ThemeBatailleNasale1.wav");
			
			try {
				clip2 = AudioSystem.getClip();
				clip2.open(AudioSystem.getAudioInputStream(audio));
				clip2.loop(Clip.LOOP_CONTINUOUSLY);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void stop() {
		if (clip != null) {
			clip.stop();
			clip = null;
		}
		if (clip2 != null) {
			clip2.stop();
			clip2 = null;
		}
	}
	
	public static void enableSons() {
		jouer = true;
	}
	
	public static void disableSons() {
		jouer = false;
		if (clip != null)
			clip.stop();
	}
	
	public static boolean isSons() {
		return jouer;
	}
}
