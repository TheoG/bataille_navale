package fr.batailleIHM.Panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import fr.batailleCtrl.MenuCtrl;
import fr.batailleCtrl.OptionCtrl;
import fr.batailleIHM.BoutonMenu;
import fr.batailleIHM.Boutons;
import fr.batailleIHM.SonManager;
import fr.batailleIHM.Sons;
import fr.batailleIHM.Fenetre.FenetreOptions;
import fr.batailleIHM.Fenetre.FenetreRegle;

/**
 * Panel du menu g�n�ral, visible au lancement de l'application
 * 
 * @author Th�o
 *
 */
public class PanelPrincipal extends JPanel {
	private static final long serialVersionUID = -1500657981683029524L;
	private MenuCtrl menuCtrl;
	private OptionCtrl optionCtrl;
	
	private BoutonMenu jb_jouer;
	private BoutonMenu jb_charger;
	private BoutonMenu jb_options;
	private BoutonMenu jb_quitter;

	private JPanel jp_boutons;
	private JLabel jl_bas;

	public PanelPrincipal(OptionCtrl optionCtrl) {
		Sons.jouerSon(SonManager.MENU);
		this.optionCtrl = optionCtrl;
		menuCtrl = new MenuCtrl(optionCtrl.renvoieRegles());
		jp_boutons = new JPanel();
		jp_boutons.setLayout(new GridLayout(4, 0));
		this.setLayout(new BorderLayout());

		// D�claration des boutons
		jb_jouer = new BoutonMenu("Commencer une partie");
		jb_charger = new BoutonMenu("Charger une partie");
		jb_options = new BoutonMenu("Régler les options");
		jb_quitter = new BoutonMenu("Quitter");
		
		// Ajout des boutons dans le panel central
		jp_boutons.add(jb_jouer);
		jp_boutons.add(jb_charger);
		jb_charger.addActionListener(new Charger());
		jp_boutons.add(jb_options);
		jp_boutons.add(jb_quitter);

		// JLabel du bas de la fenetre
		jl_bas = new JLabel("Jeu créé par Maxime Piat et Théo Gros");

		// D�claration des listeners
		jb_jouer.addActionListener(new Jouer());
		jb_jouer.addMouseListener(new BoutonHover());
		jb_options.addActionListener(new Options());
		jb_options.addMouseListener(new BoutonHover());
		jb_charger.addMouseListener(new BoutonHover());
		jb_quitter.addActionListener(new Quitter());
		jb_quitter.addMouseListener(new BoutonHover());

		// Ajout des �l�ments dans le BorderLayout
		this.setBackground(new Color(187, 222, 251));
		this.add(jp_boutons, BorderLayout.CENTER);
		this.add(jl_bas, BorderLayout.SOUTH);
		this.add(new JLabel("                                  "), BorderLayout.EAST);
		this.add(new JLabel("                                  "), BorderLayout.WEST);
		this.add(new JLabel("<html><br><br></html>"), BorderLayout.NORTH);
	}

	/**
	 * Ecouteur permettant de commencer une partie
	 * 
	 */
	public class Jouer implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			Sons.stop();
			String regles = "";
			try (BufferedReader r = new BufferedReader(new FileReader("regles.txt"))) {
				regles = r.readLine();
			} catch (IOException ie) {
				ie.printStackTrace();
			}
			menuCtrl.lancePlacementIhm();
			if (regles.equals("n"))
				new FenetreRegle();
			fermer();
		}
	}
	
	public class Options implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			new FenetreOptions(optionCtrl);
			fermer();
		}
	}

	/**
	 * Ecouteur permettant de charger une partie
	 *
	 */
	public class Charger implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fc = new JFileChooser();
			
			int valRet = fc.showOpenDialog(null);
			if (valRet == JFileChooser.APPROVE_OPTION) {
				menuCtrl.charger(fc.getSelectedFile());
				Sons.stop();
				fermer();
			}
		}
	}

	/**
	 * Ecouteur permettant de quitter l'application
	 * 
	 *
	 */
	public class Quitter implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	}
	
	public class BoutonHover implements MouseListener {

		@Override
		public void mouseClicked(java.awt.event.MouseEvent e) {
		}

		@Override
		public void mouseEntered(java.awt.event.MouseEvent e) {
			if (e.getSource() == jb_jouer) {
				new Boutons().surlumBoutons(jb_jouer);
			} else if (e.getSource() == jb_charger) {
				new Boutons().surlumBoutons(jb_charger);
			} else if (e.getSource() == jb_options) {
				new Boutons().surlumBoutons(jb_options);				
			} else if (e.getSource() == jb_quitter) {
				new Boutons().surlumBoutons(jb_quitter);
			}
		}

		@Override
		public void mouseExited(java.awt.event.MouseEvent e) {
			if (e.getSource() == jb_jouer) {
				new Boutons().quitBouton(jb_jouer);
			} else if (e.getSource() == jb_charger) {
				new Boutons().quitBouton(jb_charger);
			} else if (e.getSource() == jb_options) {
				new Boutons().quitBouton(jb_options);				
			} else if (e.getSource() == jb_quitter) {
				new Boutons().quitBouton(jb_quitter);
			}
		}

		@Override
		public void mousePressed(java.awt.event.MouseEvent e) {}
		@Override
		public void mouseReleased(java.awt.event.MouseEvent e) {}
	}
	
	public void fermer() {
		SwingUtilities.windowForComponent(this).dispose();
	}
}
