package fr.batailleIHM.Panels;


import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import fr.batailleCtrl.OptionCtrl;
import fr.batailleIHM.Sons;
import fr.batailleIHM.Fenetre.FenetrePrincipale;

public class PanelOptions extends JPanel {
	private static final long serialVersionUID = 2156879787280709627L;

	private JLabel jl_taillePlateau;
	private JComboBox<String> jcb_taillePlateau;
	private JLabel jl_nbBateaux;
	private JLabel jl_nbBateaux_2;
	private JLabel jl_nbBateaux_3;
	private JLabel jl_nbBateaux_4;
	private JLabel jl_nbBateaux_5;
	private JComboBox<String> jcb_nbre_2;
	private JComboBox<String> jcb_nbre_3;
	private JComboBox<String> jcb_nbre_4;
	private JComboBox<String> jcb_nbre_5;
	private JCheckBox jcb_sons;
	private JLabel jl_joueur1;
	private JLabel jl_joueur2;
	private JTextField jtf_joueur1;
	private JTextField jtf_joueur2;
	private JButton jb_valider;
	private JButton jb_annuler;
	private JPanel jp_principal;
	
	
	public PanelOptions(OptionCtrl optionCtrl) {
		
		// D�clarations des composants
		jl_taillePlateau = new JLabel("Taille du plateau : ");
		jl_nbBateaux = new JLabel("Nombre de bateaux maximum : 12");
		jcb_taillePlateau = new JComboBox<String>();
		jl_nbBateaux_2 = new JLabel("Nombre de bateaux de taille 2 : ");
		jl_nbBateaux_3 = new JLabel("Nombre de bateaux de taille 3 : ");
		jl_nbBateaux_4 = new JLabel("Nombre de bateaux de taille 4 : ");
		jl_nbBateaux_5 = new JLabel("Nombre de bateaux de taille 5 : ");
		jcb_nbre_2 = new JComboBox<String>();
		jcb_nbre_3 = new JComboBox<String>();
		jcb_nbre_4 = new JComboBox<String>();
		jcb_nbre_5 = new JComboBox<String>();
		jcb_nbre_2.setPreferredSize(new Dimension(50, 25));
		jcb_nbre_3.setPreferredSize(new Dimension(50, 25));
		jcb_nbre_4.setPreferredSize(new Dimension(50, 25));
		jcb_nbre_5.setPreferredSize(new Dimension(50, 25));
		jl_joueur1 = new JLabel("Nom du joueur 1 : ");
		jl_joueur2 = new JLabel("Nom du joueur 2 : ");
		jtf_joueur1 = new JTextField(10);
		jtf_joueur2 = new JTextField(10);
		jcb_sons = new JCheckBox("Jouer les sons", Sons.isSons() ? true : false);
		
		jb_valider = new JButton("Valider");
		jb_annuler = new JButton("Annuler");
		jp_principal = new JPanel(new GridLayout(3, 2, 10, 50));
		
		// Ajout des composants/options dans les combobox
		ajouterItem(8, 12, jcb_taillePlateau);
		ajouterItem(0, 3, jcb_nbre_2);
		ajouterItem(0, 3, jcb_nbre_3);
		ajouterItem(0, 3, jcb_nbre_4);
		ajouterItem(0, 3, jcb_nbre_5);
		
		jcb_taillePlateau.setSelectedIndex(optionCtrl.getTaille() - 8);
		jcb_nbre_2.setSelectedIndex(optionCtrl.getBateau2());
		jcb_nbre_3.setSelectedIndex(optionCtrl.getBateau3());
		jcb_nbre_4.setSelectedIndex(optionCtrl.getBateau4());
		jcb_nbre_5.setSelectedIndex(optionCtrl.getBateau5());
		jtf_joueur1.setText(optionCtrl.getNomJoueur1());
		jtf_joueur2.setText(optionCtrl.getNomJoueur2());
		
		// Modification des composants, ajout des ActionListener
		jb_annuler.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new FenetrePrincipale(optionCtrl);
				fermer();
			}
		});
		
		jb_valider.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int nbre_bateau2, nbre_bateau3, nbre_bateau4, nbre_bateau5;
				if (!jtf_joueur1.getText().isEmpty() && !jtf_joueur2.getText().isEmpty()) {
					nbre_bateau2 = jcb_nbre_2.getSelectedIndex();
					nbre_bateau3 = jcb_nbre_3.getSelectedIndex();
					nbre_bateau4 = jcb_nbre_4.getSelectedIndex();
					nbre_bateau5 = jcb_nbre_5.getSelectedIndex();
					if (nbre_bateau2 + nbre_bateau3 + nbre_bateau4 + nbre_bateau5 != 0) {
						optionCtrl.setBateau2(nbre_bateau2);
						optionCtrl.setBateau3(nbre_bateau3);
						optionCtrl.setBateau4(nbre_bateau4);
						optionCtrl.setBateau5(nbre_bateau5);
						optionCtrl.setNbBateau_total();
						optionCtrl.setNomJoueur1(jtf_joueur1.getText());
						optionCtrl.setNomJoueur2(jtf_joueur2.getText());
						optionCtrl.setTaille(jcb_taillePlateau.getSelectedIndex() + 8);
						new FenetrePrincipale(optionCtrl);
						fermer();
					}
				}
			}
		});
		
		jcb_sons.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					Sons.enableSons();
				} else {
					Sons.disableSons();
				}
			}
		});
				
		// Ajout des composants au panel principal
		jp_principal.add(jl_taillePlateau);
		jp_principal.add(jcb_taillePlateau);
		jp_principal.add(jl_nbBateaux);
		jp_principal.add(jb_valider);
		jp_principal.add(jb_annuler);

		
		// R�glages du panel GridBag
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		int x[] = {0, 3, 0, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 0, 3, 2, 2, 3};
		int y[] = {1, 1, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 10, 10};
		int larg[] = {3, 2, 3, 3, 1, 3, 1, 3, 1, 3, 1, 3, 2, 3, 2, 2, 2, 2};
		int haut[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2};
		int px[] = {50, 50, 80, 50, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 5};
		int py[] = {5, 5, 5, 0, 1, 1, 1, 1, 1, 1, 1, 5, 5, 5, 5, 1, 10, 10};
		JComponent element[] = {jl_taillePlateau, jcb_taillePlateau, jl_nbBateaux, jl_nbBateaux_2, jcb_nbre_2,
								jl_nbBateaux_3, jcb_nbre_3, jl_nbBateaux_4, jcb_nbre_4, jl_nbBateaux_5, jcb_nbre_5,
								jl_joueur1, jtf_joueur1, jl_joueur2, jtf_joueur2, jcb_sons, jb_valider, jb_annuler};
		
		c.fill = GridBagConstraints.NONE;
		for (int i = 0; i < element.length; i++) {
			c.gridx = x[i];
			c.gridy = y[i];
			c.gridwidth = larg[i];
			c.gridheight = haut[i];
			c.weightx = px[i];
			c.weighty = py[i];
			this.add(element[i], c);
		}
	}
	
	public void fermer() {
		SwingUtilities.windowForComponent(this).dispose();
	}

	/**
	 * Permet d'ajouter des �l�ments de type entier � une JComboBox
	 * @param min Minimum
	 * @param max Maximum
	 * @param jcb JComboBox cibl�e
	 */
	public void ajouterItem(int min, int max, JComboBox<String> jcb) {
		for (int i = min; i <= max; i++) {
			jcb.addItem(Integer.toString(i));
		}
	}
}
