package fr.batailleIHM.Panels;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import fr.batailleCtrl.OptionCtrl;
import fr.batailleCtrl.PartieCtrl;
import fr.batailleIHM.ImageManager;
import fr.batailleIHM.Images;
import fr.batailleIHM.SonManager;
import fr.batailleIHM.Sons;
import fr.batailleIHM.Fenetre.FenetrePlacement;
import fr.batailleIHM.Fenetre.FenetrePrincipale;

public class PanelPartie extends JPanel {
	private static final long serialVersionUID = -396090041860741436L;

	private PartieCtrl partieCtrl;
	private int numeroJoueur;
	private int numeroEnnemi;
	
	private JTabbedPane jtp_onglets;
	private JPanel jp_monTest;
	private JPanel jp_monPlateau;
	private JPanel jp_sonTest;
	private JPanel jp_sonPlateau;
	private JPanel jp_board;
	private JButton[][] jb_monPlateau;
	private JButton[][] jb_sonPlateau;
	private JButton jb_tourSuivant;
	private JLabel jl_nomJoueur;
	private JLabel jl_score;
	private JLabel jl_log;
	
	
	public PanelPartie(PartieCtrl partieCtrl) {
		Sons.jouerPartie();
		this.partieCtrl = partieCtrl;
		numeroJoueur = partieCtrl.joueurTour();
		numeroEnnemi = partieCtrl.joueurNonTour();
		
		jp_monPlateau = new JPanel(new GridLayout(partieCtrl.taillePlateau(), partieCtrl.taillePlateau()));
		jp_sonPlateau = new JPanel(new GridLayout(partieCtrl.taillePlateau(), partieCtrl.taillePlateau()));
		jb_monPlateau = new JButton[partieCtrl.taillePlateau()][partieCtrl.taillePlateau()];
		jb_sonPlateau = new JButton[partieCtrl.taillePlateau()][partieCtrl.taillePlateau()];
		for (int i = 0; i < partieCtrl.taillePlateau(); i++) {
			for (int j = 0; j < partieCtrl.taillePlateau(); j++) {
				jb_monPlateau[i][j] = new JButton();
				jb_sonPlateau[i][j] = new JButton();
				jb_sonPlateau[i][j].addMouseListener(new Tire(i, j));
				jb_monPlateau[i][j].setPreferredSize(new Dimension(50, 50));
				jb_sonPlateau[i][j].setPreferredSize(new Dimension(50, 50));
				jp_monPlateau.add(jb_monPlateau[i][j]);
				jp_sonPlateau.add(jb_sonPlateau[i][j]);
			}
		}
		actualise(numeroJoueur);
		actualise(numeroEnnemi);
		
		jb_tourSuivant = new JButton("Tour suivant !");
		jb_tourSuivant.setEnabled(false);
		jb_tourSuivant.addActionListener(new TourSuivant());		

		jp_board = new JPanel(new GridLayout(3, 0));
		jl_nomJoueur = new JLabel("A toi de jouer " + partieCtrl.joueurNumero(numeroJoueur).getNom());
		jl_score = new JLabel("Score : " + partieCtrl.score(numeroJoueur));
		jl_log = new JLabel();
		
		jp_board.add(jl_nomJoueur);
		jp_board.add(jl_log);
		jp_board.add(jl_score);
		
		jp_monTest = new JPanel();
		jp_monTest.add(jp_monPlateau);
		jp_sonTest = new JPanel();
		jp_sonTest.add(jp_board);
		jp_sonTest.add(jp_sonPlateau);
		jp_sonTest.add(jb_tourSuivant);
		
		jtp_onglets = new JTabbedPane(SwingConstants.TOP);

		jtp_onglets.addTab("Plateau ennemi", jp_sonTest);
		jtp_onglets.addTab("Mon plateau", jp_monTest);
		jtp_onglets.setPreferredSize(new Dimension(1200, 700));
		jtp_onglets.setOpaque(true);
		
		this.add(jtp_onglets);
	}
	
	/**
	 * M�thode permettant d'actualiser le contenu du plateau
	 * @param numeroJoueur
	 */
	public void actualise(int numeroJoueur) {
		JButton[][] jb_button;
		if (numeroJoueur == this.numeroJoueur)
			jb_button = jb_monPlateau;
		else
			jb_button = jb_sonPlateau;
		for (int ligne = 0; ligne < partieCtrl.taillePlateau(); ligne++) {
			for (int colonne = 0; colonne < partieCtrl.taillePlateau(); colonne++) {
				if (numeroJoueur == numeroEnnemi && !partieCtrl.caseEstTouche(numeroJoueur, ligne, colonne))
					jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.BRUME));
				else {
					if (partieCtrl.caseEstTouche(numeroJoueur, ligne, colonne) && !partieCtrl.estCoule(numeroJoueur, ligne, colonne) && partieCtrl.contenuCase(numeroJoueur, ligne, colonne) != 0) {
						jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.TOUCHE));
					}

					else {
						switch (partieCtrl.contenuCase(numeroJoueur, ligne, colonne)) {
						case 0 :
							double val = Math.random() * 3;
								
							if (val < 1)
								jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.EAU1));
							else if (val < 2)
								jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.EAU2));
							else
								jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.EAU3));
							break;
						case 3 :
							if (partieCtrl.estCoule(numeroJoueur, ligne, colonne))
								jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.LONG_EXTREM2_COULE));
							else if (!partieCtrl.caseEstTouche(numeroJoueur, ligne, colonne))
								jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.LONG_EXTREM2));
							break;
						case -3 :
							if (partieCtrl.estCoule(numeroJoueur, ligne, colonne))
								jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.HAUT_EXTREM2_COULE));
							else if (!partieCtrl.caseEstTouche(numeroJoueur, ligne, colonne))
								jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.HAUT_EXTREM2));
							break;
						case 2 :
							if (partieCtrl.estCoule(numeroJoueur, ligne, colonne))
								jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.LONG_EXTREM_COULE));
							else if (!partieCtrl.caseEstTouche(numeroJoueur, ligne, colonne))
								jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.LONG_EXTREM));
							break;
						case -2 :
							if (partieCtrl.estCoule(numeroJoueur, ligne, colonne))
								jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.HAUT_EXTREM_COULE));
							else if (!partieCtrl.caseEstTouche(numeroJoueur, ligne, colonne))
								jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.HAUT_EXTREM));
							break;
						case 1 :
							if (partieCtrl.estCoule(numeroJoueur, ligne, colonne))
								jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.LONG_MILIEU_COULE));
							else if (!partieCtrl.caseEstTouche(numeroJoueur, ligne, colonne))
								jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.LONG_MILIEU));
							break;
						case -1 :
							if (partieCtrl.estCoule(numeroJoueur, ligne, colonne))
								jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.HAUT_MILIEU_COULE));
							else if (!partieCtrl.caseEstTouche(numeroJoueur, ligne, colonne))
								jb_button[ligne][colonne].setIcon(ImageManager.getImage(Images.HAUT_MILIEU));
							break;
						}
					}
				}
			}
		}
	}
	
	public class Tire implements MouseListener {
		private int ligne;
		private int colonne;
		
		public Tire(int ligne, int colonne) {
			this.ligne = ligne;
			this.colonne = colonne;
		}

		@Override
		public void mouseClicked(MouseEvent arg0) {}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			setCursor(FenetrePlacement.curseurTire());
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			setCursor(FenetrePlacement.curseurNormal());
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			int resultat;
			if (!partieCtrl.caseEstTouche(numeroEnnemi, ligne, colonne) && !jb_tourSuivant.isEnabled()) {
				resultat = partieCtrl.tire(numeroJoueur, ligne, colonne);
				actualise(numeroEnnemi);
				if (resultat == 0) {
					Sons.jouerSon(SonManager.RATE);
					jl_log.setText("Plouf !");
				}
				else if (resultat == 1) {
					jl_log.setText("Touch� !");
					Sons.jouerSon(SonManager.TOUCHE);
				}
				else if (resultat == 2) {
					jl_log.setText("Coul� !");
					Sons.jouerSon(SonManager.TOUCHE);
				}
				if (partieCtrl.estFinie()) {
					JOptionPane.showMessageDialog(new JFrame(), "Bravo " + partieCtrl.nomJoueur(partieCtrl.joueurGagnant()) + " tu as gagn� !", "F�licitations !", JOptionPane.PLAIN_MESSAGE);
					jb_tourSuivant.setText("Revenir au menu");
					jb_tourSuivant.setEnabled(true);
				}
				else {
					jb_tourSuivant.setEnabled(true);
				}
			}
			jl_score.setText("Score : " + partieCtrl.score(numeroJoueur));
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {}	
	}
	
	public class TourSuivant implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ae) {
			if (!partieCtrl.estFinie()) {
				numeroJoueur = partieCtrl.joueurTour();
				numeroEnnemi = partieCtrl.joueurNonTour();
				jb_tourSuivant.setEnabled(false);
				actualise(numeroJoueur);
				actualise(numeroEnnemi);

				jl_score.setText("Score : " + partieCtrl.score(numeroJoueur));
				jl_log.setText("");
				jl_nomJoueur.setText("A toi de jouer " + partieCtrl.joueurNumero(numeroJoueur).getNom());
			}
			else {
				Sons.stop();
				new FenetrePrincipale(new OptionCtrl(partieCtrl.renvoitRegles()));
				fermer();
			}
		}
	}
	
	public void fermer() {
		SwingUtilities.windowForComponent(this).dispose();
	}
}
