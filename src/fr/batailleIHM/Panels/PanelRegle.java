package fr.batailleIHM.Panels;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class PanelRegle extends JPanel {
	private static final long serialVersionUID = 3756213571371363496L;
	
	private JPanel jp_bas;
	private JLabel image;
	private JButton jb_go;
	private JCheckBox jcb_afficher;
	
	
	public PanelRegle() {
		// Instanciations
		this.setLayout(new BorderLayout());
		jp_bas = new JPanel(new GridLayout(1, 2));
		this.setBackground(new Color(187, 222, 251));
		image = new JLabel(new ImageIcon("Ressources/Images/regles_jeu.png"));
		//
		jb_go = new JButton("C'est parti !");
		jb_go.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				fermer();
			}
		});
		//
		jcb_afficher = new JCheckBox("Ne plus afficher ce message");
		jcb_afficher.addItemListener(new Check());
		jp_bas.add(jcb_afficher);
		jp_bas.add(jb_go);
		this.add(jp_bas, BorderLayout.SOUTH);
		this.add(image, BorderLayout.NORTH);
		
	}
	
	public class Check implements ItemListener {
		@Override
		public void itemStateChanged(ItemEvent ie) {
			try (FileWriter f = new FileWriter("regles.txt")) {
				if (ie.getStateChange() == ItemEvent.SELECTED) {
					f.write("o");
				} else {
					f.write("n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void fermer() {
		SwingUtilities.windowForComponent(this).dispose();
	}
}
