package fr.batailleIHM.Panels;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;

import fr.batailleCtrl.PlacementCtrl;
import fr.batailleIHM.ImageManager;
import fr.batailleIHM.Images;
import fr.batailleIHM.SonManager;
import fr.batailleIHM.Sons;
import fr.batailleIHM.Fenetre.FenetrePlacement;

public class PanelPlacement extends JPanel {
	private static final long serialVersionUID = -3283694942625164320L;
	private PlacementCtrl placementCtrl;
	private int indice;
	private int numeroJoueur;
	
	private JPanel jp_imgBateaux;
	private JPanel jp_plateau;
	private JPanel jp_test;
	private JPanel jp_est;
	private JButton jb_valider;
	private JButton[][] tabCases;
	private JLabel[] jl_bateau;
	private JLabel[] jl_bateauRestant;
	
	public PanelPlacement(PlacementCtrl placementCtrl) {
		this.placementCtrl = placementCtrl;
		this.setLayout(new BorderLayout());
		numeroJoueur = 1;
		
		jp_est = new JPanel();
		jb_valider = new JButton("Valider");
		jb_valider.setEnabled(false);
		jb_valider.addActionListener(new Valider());
		jp_est.add(jb_valider);
		this.add(jp_est, BorderLayout.EAST);
		
		jp_plateau = new JPanel(new GridLayout(placementCtrl.taillePlateau(), placementCtrl.taillePlateau()));
		tabCases = new JButton[placementCtrl.taillePlateau()][placementCtrl.taillePlateau()];
		for (int i = 0; i < placementCtrl.taillePlateau(); i++) {
			for (int j = 0; j < placementCtrl.taillePlateau(); j++) {
				tabCases[i][j] = new JButton();
				tabCases[i][j].setPreferredSize(new Dimension(50, 50));
				tabCases[i][j].addMouseListener(new CliqueCase(i, j));
				jp_plateau.add(tabCases[i][j]);
			}
		}
		actualise();
		
		jp_test = new JPanel();
		jp_test.add(jp_plateau);
		
		this.add(jp_test, BorderLayout.CENTER);
		
		jl_bateau = new JLabel[4];
		jl_bateauRestant = new JLabel[4];
		jl_bateau[0] = new JLabel(new ImageIcon("Ressources/Images/bateau_2.png"));
		jl_bateauRestant[0] = new JLabel("" + placementCtrl.nbBateauRestant(numeroJoueur, 2));
		jl_bateau[1] = new JLabel(new ImageIcon("Ressources/Images/bateau_3.png"));
		jl_bateauRestant[1] = new JLabel("" + placementCtrl.nbBateauRestant(numeroJoueur, 3));
		jl_bateau[2] = new JLabel(new ImageIcon("Ressources/Images/bateau_4.png"));
		jl_bateauRestant[2] = new JLabel("" + placementCtrl.nbBateauRestant(numeroJoueur, 4));
		jl_bateau[3] = new JLabel(new ImageIcon("Ressources/Images/bateau_5.png"));
		jl_bateauRestant[3] = new JLabel("" + placementCtrl.nbBateauRestant(numeroJoueur, 5));

		jp_imgBateaux = new JPanel(new GridLayout(4, 2));
		for (int i = 0; i < 4; i++) {
			jl_bateau[i].addMouseListener(new CliqueBateau(i));
			jp_imgBateaux.add(jl_bateau[i]);
			jp_imgBateaux.add(jl_bateauRestant[i]);
		}	
		this.add(jp_imgBateaux, BorderLayout.WEST);
	}
	
	/**
	 * M�thode permettant d'actualiser le contenu du plateau
	 */
	public void actualise() {
		for (int i = 0; i < placementCtrl.taillePlateau(); i++) {
			for (int j = 0; j < placementCtrl.taillePlateau(); j++) {
				switch (placementCtrl.contenuCase(numeroJoueur, i, j)) {
				case 0 :
					double val = Math.random() * 3;
						
					if (val < 1)
						tabCases[i][j].setIcon(ImageManager.getImage(Images.EAU1));
					else if (val < 2)
						tabCases[i][j].setIcon(ImageManager.getImage(Images.EAU2));
					else
						tabCases[i][j].setIcon(ImageManager.getImage(Images.EAU3));
					break;
				case 3 :
					tabCases[i][j].setIcon(ImageManager.getImage(Images.LONG_EXTREM2));
					break;
				case -3 :
					tabCases[i][j].setIcon(ImageManager.getImage(Images.HAUT_EXTREM2));
					break;
				case 2 :
					tabCases[i][j].setIcon(ImageManager.getImage(Images.LONG_EXTREM));
					break;
				case -2 :
					tabCases[i][j].setIcon(ImageManager.getImage(Images.HAUT_EXTREM));
					break;
				case 1 :
					tabCases[i][j].setIcon(ImageManager.getImage(Images.LONG_MILIEU));
					break;
				case -1 :
					tabCases[i][j].setIcon(ImageManager.getImage(Images.HAUT_MILIEU));
					break;
				}
			}
		}
	}
	
	public class CliqueBateau implements MouseListener {
		private int bateau;
		
		public CliqueBateau(int bateau) {
			this.bateau = bateau;
		}
		
		@Override
		public void mouseClicked(MouseEvent arg0) {
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
		}

		@Override
		public void mouseExited(MouseEvent arg0) {}

		@Override
		public void mousePressed(MouseEvent arg0) {
			if (jl_bateau[bateau].getBorder() == null && placementCtrl.nbBateauRestant(numeroJoueur, bateau + 2) != 0) {
				for (int i = 0; i < 4; i++)
					jl_bateau[i].setBorder(null);
				jl_bateau[bateau].setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
				setCursor(FenetrePlacement.curseurPlacement());
				
				indice = placementCtrl.indiceBateauNonPlace(numeroJoueur, bateau + 2);
			}
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {}
		

	}
	
	public class CliqueCase implements MouseListener {
		private int ligne, colonne;
		
		public CliqueCase(int ligne, int colonne) {
			this.ligne = ligne;
			this.colonne = colonne;
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
		}

		@Override
		public void mousePressed(MouseEvent e) {
			boolean bateauSelectionne = false;
			int i = 0;
			while (i < 4 && !bateauSelectionne) {
				if (jl_bateau[i].getBorder() != null)
					bateauSelectionne = true;
				i++;
			}
			if (e.getButton() == MouseEvent.BUTTON1) {
				if (bateauSelectionne) {
					Sons.jouerSon(SonManager.PLACE);
					placementCtrl.placement(numeroJoueur, ligne, colonne, indice);
					actualise();
					if (placementCtrl.reserveEstPlace(numeroJoueur))
						jb_valider.setEnabled(true);
					for (i = 0; i < 4; i++)
						jl_bateau[i].setBorder(null);
					setCursor(FenetrePlacement.curseurNormal());
				}
			}
			else if (e.getButton() == MouseEvent.BUTTON3) {
				if (placementCtrl.cliqueSurBateau(numeroJoueur, ligne, colonne)) {
					Sons.jouerSon(SonManager.RETIRE);
					placementCtrl.retire(numeroJoueur, ligne, colonne);
					actualise();
					jb_valider.setEnabled(false);
				}
			}
			else if (e.getButton() == MouseEvent.BUTTON2) {
				if (placementCtrl.cliqueSurBateau(numeroJoueur, ligne, colonne)) {
					placementCtrl.tourne(numeroJoueur, ligne, colonne);
					actualise();
				}
			}
			for (i = 0; i < 4; i++)
				jl_bateauRestant[i].setText("" + placementCtrl.nbBateauRestant(numeroJoueur, i + 2));
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
		}
	}
	
	public class Valider implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (numeroJoueur == 1) {
				numeroJoueur = 2;
				actualise();
				jb_valider.setEnabled(false);
				for (int i = 0; i < 4; i++)
					jl_bateauRestant[i].setText("" + placementCtrl.nbBateauRestant(numeroJoueur, i + 2));
			}
			else if (numeroJoueur == 2) {
				placementCtrl.lancePartieIhm();
				fermer();
			}
		}
	}
	public void fermer() {
		SwingUtilities.windowForComponent(this).dispose();
	}
}
