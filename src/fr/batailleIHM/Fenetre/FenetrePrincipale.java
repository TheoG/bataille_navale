package fr.batailleIHM.Fenetre;

import fr.batailleCtrl.OptionCtrl;
import fr.batailleIHM.SonManager;
import fr.batailleIHM.Sons;
import fr.batailleIHM.SuperFenetreGenerale;
import fr.batailleIHM.Panels.PanelPrincipal;


public class FenetrePrincipale extends SuperFenetreGenerale {
	private static final long serialVersionUID = 4095447852262874334L;
	
	public FenetrePrincipale(OptionCtrl optionCtrl) {
		super(400, 400, "La bataille nasale");
		Sons.stop();
		Sons.jouerSon(SonManager.JOUER);
		this.setContentPane(new PanelPrincipal(optionCtrl));

		this.setVisible(true);
	}
}
