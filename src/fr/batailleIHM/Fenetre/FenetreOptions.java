package fr.batailleIHM.Fenetre;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import fr.batailleCtrl.OptionCtrl;
import fr.batailleIHM.SuperFenetreGenerale;
import fr.batailleIHM.Panels.PanelOptions;

public class FenetreOptions extends SuperFenetreGenerale {
	private static final long serialVersionUID = -5987214375358249629L;

	public FenetreOptions(OptionCtrl optionCtrl) {
		super(400, 600, "R�glages du jeu");
		this.setContentPane(new PanelOptions(optionCtrl));
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		
		this.setVisible(true);
	}
}
