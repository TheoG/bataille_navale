package fr.batailleIHM.Fenetre;

import fr.batailleCtrl.PartieCtrl;
import fr.batailleIHM.MenuBarJeu;
import fr.batailleIHM.SuperFenetreGenerale;
import fr.batailleIHM.Panels.PanelPartie;

public class FenetrePartie extends SuperFenetreGenerale {
	private static final long serialVersionUID = 5575076653148571451L;

	public FenetrePartie(PartieCtrl partieCtrl) {
		super(1200, partieCtrl.taillePlateau() <= 10 ? 600 : 700, "Bataille navale");
		PanelPartie panelpartie = new PanelPartie(partieCtrl);
		MenuBarJeu menu = new MenuBarJeu(null, partieCtrl);
		
		menu.enableButtons();
		
		this.setResizable(false);
		this.setJMenuBar(menu);
		this.setContentPane(panelpartie);
		
		this.setVisible(true);
	}
}