package fr.batailleIHM.Fenetre;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import fr.batailleCtrl.PlacementCtrl;
import fr.batailleIHM.MenuBarJeu;
import fr.batailleIHM.SuperFenetreGenerale;
import fr.batailleIHM.Panels.PanelPlacement;

public class FenetrePlacement extends SuperFenetreGenerale {
	private static final long serialVersionUID = 2928379522952583420L;
	private static Cursor monCurseur;
	
	public FenetrePlacement(PlacementCtrl placementCtrl) {
		super(1200, placementCtrl.taillePlateau() <= 10 ? 600 : 700, "Bataille navale");
		
		this.setJMenuBar(new MenuBarJeu(placementCtrl, null));
		this.setContentPane(new PanelPlacement(placementCtrl));
		this.setResizable(false);
		
		this.setVisible(true);
	}	
	
	/**
	 * Retourne le curseur de placement de bateau
	 * @return Curseur placement
	 */
	public static Cursor curseurPlacement() {
		Toolkit tk = Toolkit.getDefaultToolkit();
		Image img = tk.getImage("Ressources/Images/curseurAncre.png");
		monCurseur = tk.createCustomCursor(img, new Point(16, 16), "Curseur");
		
		return monCurseur;
	}
	
	/**
	 * Retourne le curseur utilis� dans la plupart des menus
	 * @return Curseur g�n�ral
	 */
	public static Cursor curseurNormal() {
		Toolkit tk = Toolkit.getDefaultToolkit();
		Image img = tk.getImage("Ressources/Images/curseurTest.png");
		monCurseur = tk.createCustomCursor(img, new Point(0, 0), "Curseur");
		
		return monCurseur;
	}
	
	/**
	 * Retourne le curseur utilis� lors d'un tir sur le plateau
	 * @return
	 */
	public static Cursor curseurTire() {
		Toolkit tk = Toolkit.getDefaultToolkit();
		Image img = tk.getImage("Ressources/Images/curseurCible.png");
		monCurseur = tk.createCustomCursor(img, new Point(16, 16), "Curseur");
		
		return monCurseur;
	}
	
	/**
	 * Permet de modifier le curseur
	 * @param curseur
	 */
	public void curseur(Cursor curseur) {
		this.setCursor(curseur);
	}
}