package fr.batailleIHM.Fenetre;

import fr.batailleIHM.SuperFenetreGenerale;
import fr.batailleIHM.Panels.PanelRegle;

public class FenetreRegle extends SuperFenetreGenerale {
	private static final long serialVersionUID = -4747581317918064132L;
	
	public FenetreRegle() {
		super(500, 700, "R�gles du jeu");

		this.setContentPane(new PanelRegle());
		this.setVisible(true);
	}
}