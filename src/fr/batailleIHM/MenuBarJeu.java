package fr.batailleIHM;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import com.sun.glass.events.KeyEvent;

import fr.batailleCtrl.MenuCtrl;
import fr.batailleCtrl.OptionCtrl;
import fr.batailleCtrl.PartieCtrl;
import fr.batailleCtrl.PlacementCtrl;
import fr.batailleIHM.Fenetre.FenetrePrincipale;
import fr.batailleIHM.Fenetre.FenetreRegle;

public class MenuBarJeu extends JMenuBar {
	private static final long serialVersionUID = 2908020372060655127L;
	
	private JMenu		jm_jeu;
	private JMenu		jm_options;
	private JMenuItem	jmi_rejouer;
	private JMenuItem	jmi_charger;
	private JMenuItem	jmi_sauvegarder;
	private JMenuItem	jmi_sauvegarder_quitter;
	private JMenuItem	jmi_quitter;
	private JMenuItem	jmi_aide;
	private PlacementCtrl placementCtrl;
	private PartieCtrl	partieCtrl;
	
	
	public MenuBarJeu(PlacementCtrl placementCtrl, PartieCtrl partieCtrl) {
		super();
		
		// Instaciation de tous les �l�ments
		jm_jeu = new JMenu("Jeu");
		jm_options = new JMenu("Options");
		//
		jmi_rejouer = new JMenuItem("Rejouer une partie");
		jmi_charger = new JMenuItem("Charger");
		jmi_sauvegarder = new JMenuItem("Sauvegarder");
		jmi_sauvegarder_quitter = new JMenuItem("Sauvegarder et quitter");
		jmi_quitter = new JMenuItem("Quitter");
		jmi_aide = new JMenuItem("Aide");
		//
		
		// Ajout des �l�ments aux menus
		jm_jeu.add(jmi_rejouer);
		jm_jeu.add(jmi_charger);
		jm_jeu.add(jmi_sauvegarder);
		jm_jeu.add(jmi_sauvegarder_quitter);
		jm_jeu.add(jmi_quitter);
		//
		jm_options.add(jmi_aide);
		
		// Ajout des mnemonics, raccourcis et r�glages des onglets
		jm_jeu.setMnemonic('J');
		jm_options.setMnemonic('O');
		//
		jmi_rejouer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_MASK));
		jmi_charger.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.CTRL_MASK));
		jmi_sauvegarder.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		jmi_quitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_MASK));
		//
		jmi_sauvegarder.setEnabled(false);
		jmi_sauvegarder_quitter.setEnabled(false);
		jmi_quitter.setToolTipText("Quitter la partie et revenir au menu");

		this.placementCtrl = placementCtrl;
		this.partieCtrl = partieCtrl;
		
		// Ajout des listeners anonymes
		jmi_quitter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Sons.stop();
				if (placementCtrl != null)
					new FenetrePrincipale(new OptionCtrl(placementCtrl.renvoitRegles()));
				else
					new FenetrePrincipale(new OptionCtrl(partieCtrl.renvoitRegles()));
				fermer();
			}
		});
		
		jmi_aide.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new FenetreRegle();
			}
		});
		
		jmi_rejouer.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				MenuCtrl menuCtrl;
				if (placementCtrl != null)
					menuCtrl = new MenuCtrl(placementCtrl.renvoitRegles());
				else
					menuCtrl = new MenuCtrl(partieCtrl.renvoitRegles());
				menuCtrl.lancePlacementIhm();
				fermer();
			}
		});
		
		jmi_sauvegarder.addActionListener(new EnregistrerFichier(false));
		jmi_sauvegarder_quitter.addActionListener(new EnregistrerFichier(true));
		jmi_charger.addActionListener(new chargerFichier());
		
		// Ajout des menus
		this.add(jm_jeu);
		this.add(jm_options);
	}
	
	public void charger(File sav) {
		if (partieCtrl != null)
			partieCtrl.charger(sav);
		else if (placementCtrl != null) {
			placementCtrl.charger(sav);
			SwingUtilities.windowForComponent(this).dispose();
		}
		repaint();
	}

	public void enregistrer(File sav) {		
		if (partieCtrl != null)
			partieCtrl.sauvegarde(sav);
	}
	
	public void enableButtons() {
		jmi_sauvegarder.setEnabled(true);
		jmi_sauvegarder_quitter.setEnabled(true);
	}
	
	public void fermer() {
		SwingUtilities.windowForComponent(this).dispose();
	}
	
	public class EnregistrerFichier implements ActionListener {
		private boolean quitter;
		
		public EnregistrerFichier(boolean quitter) {
			this.quitter = quitter;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fc = new JFileChooser();			
			
			int valRet = fc.showOpenDialog(null);
			if (valRet == JFileChooser.APPROVE_OPTION) {
				enregistrer(fc.getSelectedFile());
			}
			
			if (quitter)
				fermer();
		}
	}
	
	public class chargerFichier implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			JFileChooser fc = new JFileChooser();
			
			int valRet = fc.showOpenDialog(null);
			if (valRet == JFileChooser.APPROVE_OPTION)
				charger(fc.getSelectedFile());
		}
	}
	
	public void pouet() {
		System.out.println("pouet");
	}
}
