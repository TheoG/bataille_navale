package fr.batailleIHM;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public abstract class SuperFenetreGenerale extends JFrame {
	private static final long serialVersionUID = 5164953430146040815L;

	public SuperFenetreGenerale(int x, int y, String nom) {
		this.setTitle(nom);
		this.setSize(x, y);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setIconImage(new ImageIcon("Ressources/Images/logo.png").getImage());
		
		Toolkit tk = Toolkit.getDefaultToolkit();
		Image img = tk.getImage("Ressources/Images/curseurTest.png");
		Cursor monCurseur = tk.createCustomCursor(img, new Point(1, 1), "Curseur");
		setCursor(monCurseur);
	}
}