package fr.batailleIHM;

import javax.swing.ImageIcon;

public class ImageManager {
	private static ImageIcon eau = new ImageIcon("Ressources/Images/eau1.png");
	private static ImageIcon eau2 = new ImageIcon("Ressources/Images/eau2.png"); 
	private static ImageIcon eau3 = new ImageIcon("Ressources/Images/eau3.png");
	
	private static ImageIcon bat_long_extrem = new ImageIcon("Ressources/Images/Bateau_Long/bat_extrem.png");
	private static ImageIcon bat_haut_extrem = new ImageIcon("Ressources/Images/Bateau_Haut/bat_extrem.png");
	private static ImageIcon bat_long_extrem2 = new ImageIcon("Ressources/Images/Bateau_Long/bat_extrem2.png");
	private static ImageIcon bat_haut_extrem2 = new ImageIcon("Ressources/Images/Bateau_Haut/bat_extrem2.png");
	private static ImageIcon bat_long_milieu = new ImageIcon("Ressources/Images/Bateau_Long/bat_milieu.png");
	private static ImageIcon bat_haut_milieu = new ImageIcon("Ressources/Images/Bateau_Haut/bat_milieu.png");

	private static ImageIcon bat_long_extrem_coule = new ImageIcon("Ressources/Images/Bateau_Coule/bat_extrem_long.png");
	private static ImageIcon bat_haut_extrem_coule = new ImageIcon("Ressources/Images/Bateau_Coule/bat_extrem_haut.png");
	private static ImageIcon bat_long_extrem2_coule = new ImageIcon("Ressources/Images/Bateau_Coule/bat_extrem2_long.png");
	private static ImageIcon bat_haut_extrem2_coule = new ImageIcon("Ressources/Images/Bateau_Coule/bat_extrem2_haut.png");
	private static ImageIcon bat_long_milieu_coule = new ImageIcon("Ressources/Images/Bateau_Coule/bat_milieu_long.png");
	private static ImageIcon bat_haut_milieu_coule = new ImageIcon("Ressources/Images/Bateau_Coule/bat_milieu_haut.png");
	
	private static ImageIcon bat_explose = new ImageIcon("Ressources/Images/bat_explose.png");
	private static ImageIcon bat_touche = new ImageIcon("Ressources/Images/bat_touche.png");
	private static ImageIcon brume = new ImageIcon("Ressources/Images/brume.png");
	
	public static ImageIcon getImage(Images image) {
		ImageIcon ret;
		
		switch (image) {
		case EAU1:
			ret = eau;
			break;
		case EAU2:
			ret = eau2;
			break;
		case EAU3:
			ret = eau3;
			break;
		case LONG_EXTREM:
			ret = bat_long_extrem;
			break;
		case LONG_EXTREM2:
			ret = bat_long_extrem2;
			break;
		case HAUT_EXTREM:
			ret = bat_haut_extrem;
			break;
		case HAUT_EXTREM2:
			ret = bat_haut_extrem2;
			break;
		case HAUT_MILIEU:
			ret = bat_haut_milieu;
			break;
		case LONG_MILIEU:
			ret = bat_long_milieu;
			break;
		case LONG_EXTREM_COULE:
			ret = bat_long_extrem_coule;
			break;
		case LONG_EXTREM2_COULE:
			ret = bat_long_extrem2_coule;
			break;
		case HAUT_EXTREM_COULE:
			ret = bat_haut_extrem_coule;
			break;
		case HAUT_EXTREM2_COULE:
			ret = bat_haut_extrem2_coule;
			break;
		case HAUT_MILIEU_COULE:
			ret = bat_haut_milieu_coule;
			break;
		case LONG_MILIEU_COULE:
			ret = bat_long_milieu_coule;
			break;
		case EXPLOSE:
			ret = bat_explose;
			break;
		case TOUCHE:
			ret = bat_touche;
			break;
		case BRUME:
			ret = brume;
			break;
		default:
			ret = null;
		}
		return ret;
	}
}
